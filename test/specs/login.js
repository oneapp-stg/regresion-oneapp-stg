describe('prueba logueo mi tigo stg', () => {
    it('logueo con credenciales', async () => {
        try {
            await browser.url('https://oneapp-stg-py.tigocloud.net/ingresar');
            await browser.maximizeWindow();
            await browser.saveScreenshot('./test/capturas/log.png');
            await $('.at-button-hight-emphasis').click()


            
            const aceptarBtn = await $('#AceptarBtn');


            await browser.saveScreenshot('./test/capturas/lo1.png');
            
            await aceptarBtn.click();
            await browser.saveScreenshot('./test/capturas/log2.png');

            const emailId = await $('#email');
            
            await emailId.setValue('0981774736');

            const loginButton = await $('//*[@id="root"]/div/div/div/div[2]/div[4]/div[1]/button');
           // await loginButton.waitForDisplayed({ timeout: 10000 });
            await loginButton.click();

            const passwordField = await $('//*[@id="root"]/div/div/div/div[2]/div[3]/div/input');
           // await passwordField.waitForDisplayed({ timeout: 10000 });
            await passwordField.setValue('NuncaOlvidar30!');

            await browser.saveScreenshot('./test/capturas/prueba1.png');

            const submitButton = await $('//*[@id="root"]/div/div/div/div[2]/div[7]/div/button');
           // await submitButton.waitForDisplayed({ timeout: 10000 });
            await submitButton.click();

            const element = await $('//*[@id="openAccountsNoHeaderTitle"]/ion-item/ion-item[2]/div/span');
            await element.waitForExist({ timeout: 15000 });

            await expect(element).toBeExisting();
            await expect(element).toHaveTextContaining('Prepago');

            await browser.saveScreenshot('./test/capturas/prueba2.png');
        } catch (error) {
            console.error('Error during login test:', error);
            await browser.saveScreenshot('./test/capturas/error.png');
            throw error; // Re-throw the error to ensure the test is marked as failed
        }
    });
});
