export const config = {
    // Define el entorno de ejecución
    runner: 'local',


    waitforTimeout: 10000, // Tiempo de espera global para comandos (10 segundos)
    connectionRetryTimeout: 120000, // Tiempo de espera para conexiones (2 minutos)
    connectionRetryCount: 3, // Reintentos de conexión

    // Especifica la ruta de los archivos de prueba
    specs: [
        './test/specs/*.js'
    ],

    logLevel: 'debug',

    // Define archivos excluidos, si los hay
    exclude: [
        // 'path/to/excluded/files'
    ],

    // Configura el número máximo de instancias de WebDriverIO
    maxInstances: 10,

    // Configuración del navegador Chrome
    capabilities: [{
        maxInstances: 5,
        browserName: 'chrome',
        'goog:chromeOptions': {
            args: [
                '--headless',
                '--disable-gpu',
                '--no-sandbox',
                '--disable-dev-shm-usage',
                '--disable-setuid-sandbox',
                '--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
            ]
        }
        
    }], 

    // Nivel de detalle de los logs
    logLevel: 'info',

    // Establece si se detiene la ejecución después del primer error
    bail: 0,

    // URL base de la aplicación bajo prueba
    baseUrl: 'https://oneapp-stg-py.tigocloud.net/ingresar',

    // Tiempo máximo de espera para ciertas operaciones
    waitforTimeout: 10000,

    // Tiempo de espera para reintento de conexión
    connectionRetryTimeout: 120000,

    // Número de intentos de conexión que se realizarán
    connectionRetryCount: 3,

    // Servicios adicionales que se utilizarán
    services: ['visual'],

    // Framework de pruebas utilizado (en este caso, Mocha)
    framework: 'mocha',

    // Reportero de resultados de pruebas (en este caso, 'spec' para consola)
    reporters: ['spec'],

    // Opciones específicas de Mocha
    mochaOpts: {
        ui: 'bdd',       // Interfaz de usuario utilizada por Mocha
        timeout: 60000   // Tiempo de espera máximo para una prueba
    },

    // Hooks personalizados para manejar eventos específicos del ciclo de vida de las pruebas
    /**
     * Gets executed once before all workers get launched.
     * @param {object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     */
    // onPrepare: function (config, capabilities) {
    // },

    /**
     * Gets executed before test execution begins.
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
    // before: function (capabilities, specs) {
    // },

    /**
     * Function to be executed after a test (in Mocha/Jasmine only)
     * @param {object}  test             test object
     * @param {object}  context          scope object the test was executed with
     * @param {Error}   result.error     error object in case the test fails, otherwise `undefined`
     * @param {*}       result.result    return object of test function
     * @param {number}  result.duration  duration of test
     * @param {boolean} result.passed    true if test has passed, otherwise false
     * @param {object}  result.retries   information about spec related retries, e.g. `{ attempts: 0, limit: 0 }`
     */
    // afterTest: function(test, context, { error, result, duration, passed, retries }) {
    // },

    /**
     * Gets executed after all workers got shut down and the process is about to exit.
     * @param {object} exitCode 0 - success, 1 - fail
     * @param {object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {<Object>} results object containing test results
     */
    // onComplete: function(exitCode, config, capabilities, results) {
    // },
};
